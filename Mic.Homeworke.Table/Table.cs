﻿using System;
namespace Mic.Homeworke.Table
{
    class Table
    {
        static byte n = 0;
        public Table()
        {
            _row = 2;
            _row = 5;
            n++;
        }
        private byte _row;
        public byte Row
        {
            get { return _row; }
            set
            {
                if (value != 0)
                    _row = value;
            }
        }
        private byte _column;
        public byte Column
        {
            get { return _column; }
            set
            {
                if (value != 0)
                    _column = value;
            }
        }
        public  void Print()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Table N{n}");
            Console.ForegroundColor = ConsoleColor.Green;
            for (int i = 0; i < Row ; i++)
            {
                if(i == 0)
                for (int y = 0; y < Column; y++)
                {
                    Console.Write(" --  ");
                }
                Console.WriteLine();
                for (int j = 0; j < Column; j++)
                {
                    Console.Write("|  | ");
                }
                Console.WriteLine();
                for (int y = 0; y < Column; y++)
                {
                    Console.Write(" --  ");
                }
            }
            Console.WriteLine();
            Console.ResetColor();
        }
    }
}
