﻿using System;

namespace Mic.Homeworke.Table
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            var t1 = new Table()
            {
                Row = 4,
                Column = 7
            };
            t1.Print();
            var t2 = new Table()
            {
                Row = 2,
                Column = 5
            };
            t2.Print();
        }
    }
}
